# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-11-05 07:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parts', '0017_auto_20171102_1415'),
    ]

    operations = [
        migrations.AddField(
            model_name='assemblydone',
            name='responce_code',
            field=models.IntegerField(default=200),
        ),
    ]
