# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-11-01 10:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('parts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Displacement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('url', models.CharField(max_length=1024)),
                ('bike_model', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='parts.BikeModel')),
            ],
        ),
    ]
