# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-11-01 18:06
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('parts', '0006_auto_20171101_1803'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='SpecificationSheet',
            new_name='SpecificationSheetsPage',
        ),
    ]
