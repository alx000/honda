import requests
from stem import Signal
from stem.control import Controller
import json
from time import sleep
import logging
from django.utils.timezone import now
from django.conf import settings
import os

logger = logging.getLogger('')


class ControlledSession:
    session = None
    last_ip = None
    locked = False
    old_ip = None
    control_port = None
    password = None
    _proxies = None
    socket_timeout = None
    slow_timeout = None
    _num_slow = 0
    _max_num_slow = 10

    def __init__(self, host='127.0.0.1', port='9050', control_port=9051, password='', socket_timeout=10,
                 slow_timeout=2):
        self.session = requests.session()
        url = 'socks5://%s:%s' % (host, port,)
        self._proxies = {'http': url, 'https': url}
        self.session.proxies = self._proxies
        self.control_port = control_port
        self.password = password
        self.socket_timeout = socket_timeout
        self.slow_timeout = slow_timeout

    def get(self, url, **kwargs):
        if 'timeout' in kwargs:
            del kwargs['timeout']
        while True:
            try:
                ts = now()
                res = self.session.get(url, timeout=self.socket_timeout, **kwargs)
                if (now() - ts).seconds > self.slow_timeout:
                    self._num_slow += 1
                    if self._num_slow > self._max_num_slow:
                        self.change_ip()
                if res.status_code == 200:
                    return res
                self.change_ip()
            except:
                sleep(0.5)

    def _renew_connection(self):
        with Controller.from_port(port=self.control_port) as controller:
            controller.authenticate(password=self.password)
            controller.signal(Signal.NEWNYM)
        self.session = requests.session()
        self.session.proxies = self._proxies

    def change_ip(self):
        if self.locked:
            sleep(1)
            return
        self.locked = True
        if self.old_ip is None:
            while True:
                try:
                    resp = self.session.get('http://api.ipify.org/?format=json')
                    break
                except:
                    sleep(0.1)
            self.old_ip = json.loads(resp.text)['ip']
        ip = self.old_ip
        while True:
            try:
                self._renew_connection()
                break
            except:
                sleep(5)
        while ip == self.old_ip:
            sleep(1)
            while True:
                try:
                    resp = self.session.get('http://api.ipify.org/?format=json')
                    break
                except:
                    sleep(0.1)
            ip = json.loads(resp.text)['ip']
        logging.warning('IP changed from %s to %s' % (self.old_ip, ip,))
        self.old_ip = ip
        self.locked = False


