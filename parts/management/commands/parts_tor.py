from django.core.management.base import BaseCommand
import requests
from bs4 import BeautifulSoup
from parts.models import Assembly, AssemblyImage, AssemblyDone, AssemblyLocked
from django.db import connection
from contextlib import closing
from django.db.utils import ProgrammingError
import logging
from time import sleep
import concurrent.futures
import sys
import traceback
from django.utils.timezone import now
from django.db.utils import IntegrityError
from parts.utils import ControlledSession
"""
apt install apparmor-utils
aa-complain system_tor
systemctl restart tor
"""


logger = logging.getLogger('')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

sess = ControlledSession(host='127.0.0.1', port='9050', control_port=9051, password='112211', slow_timeout=1)


def get_parts(asm):
    ts = now()
    while True:
        try:
            res = sess.get(asm.url)
            if res.status_code == 200:
                break
        except:
            sleep(0.5)

    soup = BeautifulSoup(res.text, 'html5lib')
    img = soup.find('img', attrs={'class': 'axzoomer'})
    if img is not None:
        AssemblyImage.objects.create(assembly=asm, url=img['src-big'])
    data = []
    c = 0
    for cnt in soup.select('.verifidentification_v2_new_table tr'):
        c += 1
        if c == 1:
            continue
        cells = cnt.select('td')
        item = [asm.id, cells[0].text.strip(),
                cells[1].text.strip(),
                cells[2].text.strip(),
                cells[3].text.strip(),
                cells[4].find('input')['value'],
                'https://www.bike-parts-honda.com/' + cells[2].find('a')['href']
                ]
        data.append(item)
    sql = "INSERT into parts_part (assembly_id, row_number, \"name\", part_number, price, qty, compat_url) VALUES {}"
    fmt = ", ".join(['(%s, %s, %s, %s, %s, %s, %s)'] * len(data))
    sql = sql.format(fmt)
    raw_values = []
    for _x in data:
        raw_values += _x
    if len(data) > 0:
        with closing(connection.cursor()) as cursor:
            try:
                cursor.execute(sql, tuple(raw_values))
                # print(cursor.mogrify(sql, tuple(raw_values)).decode())
                logging.info('%s parts created for assembly #%s' % (len(data), asm.id,))
            except ProgrammingError as e:
                print(e)
                exit()

    try:
        AssemblyDone.objects.create(assembly=asm)
    except IntegrityError:
        pass
    logger.info('Assembly #%s done in %s' % (asm.id, now()-ts,))


class Command(BaseCommand):
    def handle(self, *args, **options):
        while True:
            ts = now()
            qs = Assembly.objects.filter(locked__isnull=True, done__isnull=True)[:200]
            if len(qs) == 0:
                break
            _locked = []
            for m in qs:
                _locked.append(AssemblyLocked(assembly=m))
            try:
                AssemblyLocked.objects.bulk_create(_locked)
            except:
                sleep(2)
                continue
                # get_assemblies(m)
                logger.info('Threads started, %s items' % (len(qs),))
            with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
                future_to_lst = {executor.submit(get_parts, item): item for item in qs}
                for future in concurrent.futures.as_completed(future_to_lst):
                    future_to_lst[future]
                    try:
                        future.result()
                    except Exception as exc:
                        print('generated an exception: %s type: %s' % (exc, type(exc),))
                        print('\n')
                        raise sys.exc_info()[0](traceback.format_exc())
            logger.info('Threads complete is %s' % (now() - ts,))


