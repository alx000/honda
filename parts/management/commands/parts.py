from django.core.management.base import BaseCommand
import requests
from bs4 import BeautifulSoup
from parts.models import Assembly, AssemblyImage, AssemblyDone, AssemblyLocked
from django.db import connection
from contextlib import closing
from django.db.utils import ProgrammingError
import logging
from time import sleep
import concurrent.futures
import sys
import traceback
from django.utils.timezone import now
from django.db.utils import IntegrityError

logger = logging.getLogger('')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def get_parts(asm):
    ts = now()
    logger.info('started assembly #%s, url %s' % (asm.id, asm.url,))
    try:
        res = requests.get(asm.url)
    except Exception as e:
        logger.error('%s - %s' % (e, type(e),))
        return
    if not res.status_code == 200:
        if res.status_code in [404, 403]:
            AssemblyDone.objects.create(assembly=asm, responce_code=res.status_code)
        logger.warning('assembly #%s, url %s returned code %s' % (asm.id, asm.url, res.status_code,))
        return
    soup = BeautifulSoup(res.text, 'html5lib')
    img = soup.find('img', attrs={'class': 'axzoomer'})
    if img is not None:
        AssemblyImage.objects.create(assembly=asm, url=img['src-big'])
    data = []
    c = 0
    for cnt in soup.select('.verifidentification_v2_new_table tr'):
        c += 1
        if c == 1:
            continue
        cells = cnt.select('td')
        item = [asm.id, cells[0].text.strip(),
                cells[1].text.strip(),
                cells[2].text.strip(),
                cells[3].text.strip(),
                cells[4].find('input')['value'],
                'https://www.bike-parts-honda.com/' + cells[2].find('a')['href']
                ]
        data.append(item)
    sql = "INSERT into parts_part (assembly_id, row_number, \"name\", part_number, price, qty, compat_url) VALUES {}"
    fmt = ", ".join(['(%s, %s, %s, %s, %s, %s, %s)'] * len(data))
    sql = sql.format(fmt)
    raw_values = []
    for _x in data:
        raw_values += _x
    if len(data) > 0:
        with closing(connection.cursor()) as cursor:
            try:
                cursor.execute(sql, tuple(raw_values))
                # print(cursor.mogrify(sql, tuple(raw_values)).decode())
                logging.info('%s parts created for assembly #%s' % (len(data), asm.id,))
            except ProgrammingError as e:
                print(e)
                exit()

    try:
        AssemblyDone.objects.create(assembly=asm)
    except IntegrityError:
        pass
    logger.info('Assembly #%s done in %s' % (asm.id, now()-ts,))


class Command(BaseCommand):
    def handle(self, *args, **options):
        while True:
            ts = now()
            logger.info('Locking objects')
            qs = Assembly.objects.filter(locked__isnull=True, done__isnull=True)[:1000]
            if len(qs) == 0:
                break
            try:
                _locked = []
                sql = "INSERT INTO parts_assemblylocked (assembly_id, ts) VALUES {}"
                for m in qs:
                    _locked.append([m.id, now()])
                fmt = ", ".join(['(%s, %s)'] * len(_locked))
                sql = sql.format(fmt)
                raw_values = []
                for _x in _locked:
                    raw_values += _x
                if len(_locked) > 0:
                    with closing(connection.cursor()) as cursor:
                        cursor.execute(sql, tuple(raw_values))
                        # print(cursor.mogrify(sql, tuple(raw_values)).decode())
            except:
                logger.warning('cannot lock objects. waiting')
                sleep(5)
                continue
            logger.info('Threads started, %s items' % (len(qs),))
            with concurrent.futures.ThreadPoolExecutor(max_workers=7) as executor:
                future_to_lst = {executor.submit(get_parts, item): item for item in qs}
                for future in concurrent.futures.as_completed(future_to_lst):
                    future_to_lst[future]
                    try:
                        future.result()
                    except Exception as exc:
                        print('generated an exception: %s type: %s' % (exc, type(exc),))
                        print('\n')
                        raise sys.exc_info()[0](traceback.format_exc())
            logger.info('Threads complete is %s' % (now() - ts,))
        logger.info('job finished')


