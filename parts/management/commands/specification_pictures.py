from django.core.management.base import BaseCommand
import requests
from bs4 import BeautifulSoup
from parts.models import (SpecificationSheetsPage, SpecificationSheetsPicture, SpecificationSheetsPageLocked,
                          SpecificationSheetsPageDone)
from django.db import connection
from contextlib import closing
from django.db.utils import ProgrammingError
import logging
from time import sleep
import concurrent.futures
import sys
import traceback
from django.utils.timezone import now

logger = logging.getLogger('')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def get_pictures(page):
    ts = now()
    res = requests.get(page.page_url)
    soup = BeautifulSoup(res.text, 'html5lib')
    try:
        page.some_text = soup.find('div', attrs={'class': 'ContTxtFicheAccueil'}).text
        page.save()
    except:
        pass
    create = []
    for link in soup.select('#ContFolder a'):
        res = requests.get('https://www.bike-parts-honda.com/' + link['href'])
        soup = BeautifulSoup(res.text, 'html5lib')
        for x in soup.select('img.axzoomer'):
            create.append(SpecificationSheetsPicture(make_id=page.make_id,
                                                     url='https://www.bike-parts-honda.com/' + x['src']))
    SpecificationSheetsPicture.objects.bulk_create(create)
    SpecificationSheetsPageDone.objects.create(page=page)
    logger.info('Specification page #%s done in %s, %s pictures' % (page.id, now() - ts, len(create),))


class Command(BaseCommand):
    def handle(self, *args, **options):
        while True:
            ts = now()
            qs = SpecificationSheetsPage.objects.filter(locked__isnull=True, done__isnull=True)[:200]
            if len(qs) == 0:
                break
            _locked = []
            for p in qs:
                _locked.append(SpecificationSheetsPageLocked(page=p))
            try:
                SpecificationSheetsPageLocked.objects.bulk_create(_locked)
            except:
                sleep(2)
                continue
            logger.info('Threads started, %s items' % (len(qs),))
            with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
                future_to_lst = {executor.submit(get_pictures, item): item for item in qs}
                for future in concurrent.futures.as_completed(future_to_lst):
                    future_to_lst[future]
                    try:
                        future.result()
                    except Exception as exc:
                        print('generated an exception: %s type: %s' % (exc, type(exc),))
                        print('\n')
                        raise sys.exc_info()[0](traceback.format_exc())
            logger.info('Threads complete is %s' % (now() - ts,))
            # return
