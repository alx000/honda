from django.core.management.base import BaseCommand
import requests
from bs4 import BeautifulSoup
from parts.models import Year, Make


class Command(BaseCommand):

    def handle(self, *args, **options):
        for d in Year.objects.all():
            print(d.url)
            res = requests.get(d.url)
            soup = BeautifulSoup(res.text, 'html5lib')
            for r in soup.select('.verifidentification_v2_new_table tbody tr'):
                cells = r.select('td')
                if len(cells) < 2:
                    continue
                desc = str(cells[1]).replace('<td height="25">', '').replace('</td>', '').strip()
                _parts = [x.strip() for x in desc.split('<br/>')]
                parts = []
                for z in _parts:
                    if '<a' in z:
                        continue
                    parts.append(z.strip())
                defs = {
                    'url': cells[0].find('a')['href'],
                    'year': d,
                    'name': cells[1].find('a').text.strip(),
                    'code': parts[0],
                    'year_text': parts[1] if len(parts) > 0 else None,
                    'location': parts[2] if len(parts) > 1 else None,
                }
                try:
                    defs['thumb_url'] = cells[0].find('img')['src']
                except:
                    pass
                Make.objects.get_or_create(**defs)

