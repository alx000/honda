from django.core.management.base import BaseCommand
import requests
from bs4 import BeautifulSoup
from parts.models import Year, Displacement


class Command(BaseCommand):

    def handle(self, *args, **options):
        for d in Displacement.objects.all():
            res = requests.get(d.url)
            soup = BeautifulSoup(res.text, 'html5lib')
            for x in soup.select('.niveau3 a'):
                print(x['href'], x.text)
                y = Year.objects.get_or_create(url='https://www.bike-parts-honda.com/' + x['href'], name=x.text,
                                               displacement=d)
                print(y)


