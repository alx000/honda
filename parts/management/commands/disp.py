from django.core.management.base import BaseCommand
import requests
from bs4 import BeautifulSoup
from parts.models import BikeModel, Displacement


class Command(BaseCommand):

    def handle(self, *args, **options):
        for vm in BikeModel.objects.all():
            print(vm)
            res = requests.get(vm.url)
            soup = BeautifulSoup(res.text, 'html5lib')
            for x in soup.select('.niveau2 a'):
                print(x['href'], x.text)
                d = Displacement.objects.get_or_create(bike_model=vm, name=x.text,
                                                       url='https://www.bike-parts-honda.com/' + x['href'])
                print(d)


