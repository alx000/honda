from django.core.management.base import BaseCommand
import requests
from bs4 import BeautifulSoup
from parts.models import Make, MakeDone, MakeLocked, SpecificationSheetsPage
from django.db import connection
from contextlib import closing
from django.db.utils import ProgrammingError
import logging
from time import sleep
import concurrent.futures
import sys
import traceback
from django.utils.timezone import now
from django.db.utils import IntegrityError

logger = logging.getLogger('')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def get_assemblies(make):
    ts = now()
    res = requests.get('https://www.bike-parts-honda.com/' + make.url)
    soup = BeautifulSoup(res.text, 'html5lib')
    data = []
    for cnt in soup.select('.microfiche_mini_200'):
        item = [make.id, cnt.find('div', {'class': 'text_200'}).text.strip(),
                'https://www.bike-parts-honda.com/' + cnt.find('div', {'class': 'text_200'}).find('a')['href'],
                'https://www.bike-parts-honda.com/' + cnt.find('img')['src']
                ]
        data.append(item)
    sql = "INSERT into parts_assembly (make_id, \"name\", url, thumb_url) VALUES {}"
    fmt = ", ".join(['(%s, %s, %s, %s)'] * len(data))
    sql = sql.format(fmt)
    raw_values = []
    for _x in data:
        raw_values += _x
    if len(data) > 0:
        with closing(connection.cursor()) as cursor:
            try:
                cursor.execute(sql, tuple(raw_values))
                # print(cursor.mogrify(sql, tuple(raw_values)).decode())
                logging.info('%s assemblies created for make #%s' % (len(data), make.id,))
            except ProgrammingError as e:
                print(e)
                exit()
    for x in soup.select('a.art-indexbutton'):
        if 'See specification sheet' in x.text:
            url = 'https://www.bike-parts-honda.com/' + x['href']
            SpecificationSheetsPage.objects.create(page_url=url, make=make)
            logger.info('Specifications found for make %s' % (make.id,))
            break
    try:
        MakeDone.objects.create(make=make)
    except IntegrityError:
        pass
    logger.info('Make #%s done in %s' % (make.id, now()-ts,))


class Command(BaseCommand):
    def handle(self, *args, **options):
        while True:
            ts = now()
            qs = Make.objects.filter(locked__isnull=True, done__isnull=True)[:50]
            if len(qs) == 0:
                break
            _locked = []
            for m in qs:
                _locked.append(MakeLocked(make=m))
                try:
                    MakeLocked.objects.bulk_create(_locked)
                except:
                    sleep(2)
                    break
                # get_assemblies(m)
                logger.info('Threads started, %s items' % (len(qs),))
                with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
                    future_to_lst = {executor.submit(get_assemblies, item): item for item in qs}
                    for future in concurrent.futures.as_completed(future_to_lst):
                        future_to_lst[future]
                        try:
                            future.result()
                        except Exception as exc:
                            print('generated an exception: %s type: %s' % (exc, type(exc),))
                            print('\n')
                            raise sys.exc_info()[0](traceback.format_exc())
                logger.info('Threads complete is %s' % (now() - ts,))
