from django.core.management.base import BaseCommand
import requests
from bs4 import BeautifulSoup
from parts.models import Make, MakeDone, MakeLocked, MakeType, MakeColor
import logging
from time import sleep
import concurrent.futures
import sys
import traceback
from django.utils.timezone import now
from django.db.utils import IntegrityError

logger = logging.getLogger('')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def get_make_features(make):
    ts = now()
    res = requests.get('https://www.bike-parts-honda.com/' + make.url)
    soup = BeautifulSoup(res.text, 'html5lib')
    tbl = soup.select('.table_identification_T1')[0]
    vtype = None
    for cell in tbl.select('td'):
        if 'Type :' in str(cell):
            vtype = cell.find('span', {'class': 'titre_12_red'})
            break
    MakeType.objects.get_or_create(make=make, type=vtype.text.strip())
    tbl = soup.select('.table_identification_T1')[1]
    MakeColor.objects.filter(make=make).delete()
    _create = []
    for cell in tbl.select('td'):
        modal = cell.find('img', {'class': 'modalgallery'})
        color = cell.find('span', {'class': 'Texte_arial_bold_12_noir'})
        defs = {
            'picture': None if modal is None else 'https://www.bike-parts-honda.com/' + modal['src'],
            'color': None if color is None else color.text.strip(),
            'make': make
        }
        _create.append(MakeColor(**defs))
    MakeColor.objects.bulk_create(_create)
    try:
        MakeDone.objects.create(make=make)
    except IntegrityError:
        pass
    logger.info('Make #%s done in %s' % (make.id, now()-ts,))


class Command(BaseCommand):
    def handle(self, *args, **options):
        while True:
            ts = now()
            qs = Make.objects.filter(locked__isnull=True, done__isnull=True)[:50]
            if len(qs) == 0:
                break
            _locked = []
            for m in qs:
                _locked.append(MakeLocked(make=m))
                try:
                    MakeLocked.objects.bulk_create(_locked)
                except:
                    sleep(2)
                    break
                # get_assemblies(m)
                logger.info('Threads started, %s items' % (len(qs),))
                with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
                    future_to_lst = {executor.submit(get_make_features, item): item for item in qs}
                    for future in concurrent.futures.as_completed(future_to_lst):
                        future_to_lst[future]
                        try:
                            future.result()
                        except Exception as exc:
                            print('generated an exception: %s type: %s' % (exc, type(exc),))
                            print('\n')
                            raise sys.exc_info()[0](traceback.format_exc())
                logger.info('Threads complete is %s' % (now() - ts,))
            

