from django.core.management.base import BaseCommand
import requests
from bs4 import BeautifulSoup
from parts.models import BikeModel

class Command(BaseCommand):

    def handle(self, *args, **options):
        res = requests.get('https://www.bike-parts-honda.com/index.php')
        soup = BeautifulSoup(res.text, 'html5lib')

        for x in soup.select('.art-indexhmenu li a'):
            print(x, type(x))
            print(x.text, x['href'])
            BikeModel.objects.get_or_create(name=x.text, url='https://www.bike-parts-honda.com/' + x['href'])

