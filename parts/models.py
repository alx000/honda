from django.db import models
from django.utils.timezone import now


class BikeModel(models.Model):
    name = models.CharField(max_length=255)
    url = models.CharField(max_length=1024)

    def __str__(self):
        return '%s - %s' % (self.name, self.url,)


class Displacement(models.Model):
    bike_model = models.ForeignKey(BikeModel)
    name = models.CharField(max_length=255)
    url = models.CharField(max_length=1024)

    def __str__(self):
        return '%s - %s - %s' % (self.bike_model.name, self.name, self.url,)


class Year(models.Model):
    displacement = models.ForeignKey(Displacement)
    name = models.CharField(max_length=255)
    url = models.CharField(max_length=1024)

    def __str__(self):
        return '%s - %s - %s - %s' % (self.displacement.bike_model.name, self.displacement.name,
                                      self.name, self.url)


class Make(models.Model):
    year = models.ForeignKey(Year)
    name = models.CharField(max_length=255)
    url = models.CharField(max_length=1024)
    code = models.CharField(max_length=255)
    year_text = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    thumb_url = models.CharField(max_length=255)


class MakeType(models.Model):
    make = models.OneToOneField(Make)
    type = models.CharField(max_length=255)


class MakeColor(models.Model):
    make = models.ForeignKey(Make)
    picture = models.CharField(max_length=1024, null=True)
    color = models.CharField(max_length=255, null=True)


class MakeLocked(models.Model):
    ts = models.DateTimeField(default=now)
    make = models.OneToOneField(Make, related_name='locked')


class MakeDone(models.Model):
    ts = models.DateTimeField(default=now)
    make = models.OneToOneField(Make, related_name='done')


class SpecificationSheetsPage(models.Model):
    make = models.ForeignKey(Make)
    page_url = models.CharField(max_length=1024)
    some_text = models.TextField(null=True)


class SpecificationSheetsPageLocked(models.Model):
    page = models.OneToOneField(SpecificationSheetsPage, related_name='locked')
    ts = models.DateTimeField(default=now)


class SpecificationSheetsPageDone(models.Model):
    page = models.OneToOneField(SpecificationSheetsPage, related_name='done')
    ts = models.DateTimeField(default=now)


class SpecificationSheetsPicture(models.Model):
    make = models.ForeignKey(Make)
    url = models.CharField(max_length=1024)


class Assembly(models.Model):
    make = models.ForeignKey(Make)
    name = models.CharField(max_length=1024)
    url = models.CharField(max_length=1024)
    thumb_url = models.CharField(max_length=1024)


class AssemblyImage(models.Model):
    assembly = models.ForeignKey(Assembly)
    url = models.CharField(max_length=255)


class AssemblyLocked(models.Model):
    assembly = models.OneToOneField(Assembly, related_name='locked')
    ts = models.DateTimeField(default=now)


class AssemblyDone(models.Model):
    assembly = models.OneToOneField(Assembly, related_name='done')
    ts = models.DateTimeField(default=now)
    responce_code = models.IntegerField(default=200)


class Part(models.Model):
    row_number = models.CharField(max_length=255, null=True)
    assembly = models.ForeignKey(Assembly)
    name = models.CharField(max_length=255)
    part_number = models.CharField(max_length=255)
    price = models.CharField(max_length=255, null=True)
    qty = models.IntegerField(null=True)
    compat_url = models.CharField(max_length=255)
    # ts = models.DateTimeField(default=now)
